LANGUAGE = {
	["zh-cn"] = {
		["sln/popup/open"] = "打开";
		["sln/popup/new"] = "新建";
		["sln/popup/new/file"] = "文件";
		["sln/popup/new/folder"] = "目录";
		["sln/popup/open_file_dir"] = "打开文件位置";
		["sln/popup/open_terminal"] = "打开命令行";
		["sln/popup/close_files_in_dir"] = "关闭已打开文件";
		["sln/popup/cut"] = "剪切(&X)";
		["sln/popup/copy"] = "复制(&C)";
		["sln/popup/paste"] = "粘贴(&V)";
		["sln/popup/delete"] = "删除(&D)";
		["sln/popup/rename"] = "重命名(&R)";
		["sln/popup/property"] = "属性(&A)";

		["text/popup/jump_to_file"] = "转到文件(&F)";
		["text/popup/jump_to_definition"] = "转到定义/声明(&D)";
		["text/popup/find_implementation"] = "查找实现";
		["text/popup/find_reference"] = "查找引用";
		["text/popup/close_other_files"] = "关闭其它文件";
		["text/popup/open_directory"] = "打开文件位置";
		["text/popup/open_terminal"] = "打开命令行";
		["text/popup/cut"] = "剪切(&X)";
		["text/popup/copy"] = "复制(&C)";
		["text/popup/paste"] = "粘贴(&V)";
		["text/popup/select_all"] = "全选(&A)";
		["sln/popup/git"] = "GIT";
		["sln/popup/git/pull"] = "更新";
		["sln/popup/git/push"] = "推送";
		["sln/popup/git/history"] = "历史";
		["sln/popup/git/gui"] = "GUI";

		["utils/file_format_popup/reload_as"] = "重新加载为";
		["utils/file_format_popup/save_as"] = "保存为";
		
		["notify/build/still_running"] = "正在构建...";
		["notify/build/success"] = "构建成功!";
		["notify/build/failed"] = "构建失败!";
		["notify/build/installed"] = "安装成功!";
		["notify/build/install_failed"] = "安装失败!";
		["notify/build/cmake_cache_rebuilt"] = "CMake缓存重建成功!";
		["notify/build/cmake_cache_rebuild_failed"] = "CMake缓存重建失败!";
		["notify/build/cmake_cache_refreshed"] = "CMake缓存更新成功!";
		["notify/build/cmake_cache_refresh_failed"] = "CMake缓存更新失败!";
		
		["notify/debug/invalid_target"] = "无效的调试目标!";
		["notify/debug/target_not_exists"] = "调试目标不存在!";

		["notify/register_shortcuts/success"] = "关联右键菜单成功！";
		["notify/register_shortcuts/failed"] = "关联右键菜单失败！";
		["notify/error"] = "错误";
		["notify/error/open_file_failed"] = "打开文档失败!";
		["notify/error/save_file_failed"] = "保存文档失败!";
		["notify/error/reload_file_failed"] = "重新加载文档失败!";
		["notify/error/update_failed"] = "更新失败! 稍候请重试!";

		["notify/notice"] = "提示";
		["notify/notice/save_changed_file"] = "文档[%ls]已经更改, 是否保存?";
		["notify/notice/overwrite_readonly_file"] = "文档为只读状态, 是否覆盖?";
		["notify/notice/create_new_file"] = "文档件不存在, 是否创建?";
		["notify/notice/searching_text_not_found"] = "未找到匹配的文本:\n\n";

		["notify/invalid_license"] = "授权无效! 程序将要退出!";
		["notify/license_expires"] = "授权已经过期! 程序将要退出!";
		["notify/internal_error"] = "内部错误! 程序将要退出!";
		["notify/edx_out_of_data"] = "EDX已经过期。程序将要退出!";
		["notify/found_new_version"] = "发现新版本 %ls!";
		["notify/update_now"] = "EDX已经准备好更新。";
		["notify/old_target_not_found"] = "当前项目[%s]无效！";
		["notify/new_target_selected"] = "已经选中新项目[%s]。";

		["menu_bar/file"] = "文件";
		["menu_bar/file"] = "文件";
		["menu_bar/file/new_file"] = "新建(&N)";
		["menu_bar/file/open"] = "打开(&O)...";
		["menu_bar/file/open_in_current_buffer"] = "打开到当前窗口(&W)...";
		["menu_bar/file/close_file"] = "关闭(&C)";
		["menu_bar/file/close_project"] = "关闭解决方案";
		["menu_bar/file/save"] = "保存(&S)";
		["menu_bar/file/save_as"] = "另存为(&A)...";
		["menu_bar/file/save_all"] = "全部保存";
		["menu_bar/file/open_config_file"] = "编辑配置文件";
		["menu_bar/file/recently_files"] = "最近使用的文件...";
		["menu_bar/file/recently_projects"] = "最近使用的工程...";
		["menu_bar/file/exit"] = "退出(&X)";

		
		["menu_bar/edit"] = "编辑";
		["menu_bar/edit/undo"] = "撤销(&U)";
		["menu_bar/edit/redo"] = "重做(&R)";
		["menu_bar/edit/cut"] = "剪切(&X)";
		["menu_bar/edit/copy"] = "复制(&C)";
		["menu_bar/edit/paste"] = "粘贴(&P)";
		["menu_bar/edit/circulate_paste"] = "循环粘贴";
		["menu_bar/edit/delete"] = "删除(&D)";
		["menu_bar/edit/select_all"] = "全选(&A)";
		["menu_bar/edit/find"] = "查找/替换(&S)";
		["menu_bar/edit/find/incremental"] = "渐进查找(&I)...";
		["menu_bar/edit/find/find_and_replace"] = "查找/替换(&F)...";
		["menu_bar/edit/find/find_in_files"] = "多文件查找/替换...";
		["menu_bar/edit/jump_to"] = "跳转(&J)";
		["menu_bar/edit/jump_to/line"] = "转到行(&G)...";
		["menu_bar/edit/jump_to/previous_location"] = "前一个位置";
		["menu_bar/edit/jump_to/next_location"] = "后一个位置";
		["menu_bar/edit/jump_to/matched_brackets"] = "转到匹配括号";
		["menu_bar/edit/jump_to/file"] = "转到文件";
		["menu_bar/edit/jump_to/definition"] = "转到定义/声明";
		["menu_bar/edit/jump_to/implementation"] = "查找实现";
		["menu_bar/edit/jump_to/reference"] = "查找引用";
		["menu_bar/edit/outline"] = "代码大纲(&O)";
		["menu_bar/edit/outline/fold_unfold"] = "折叠/展开代码";
		["menu_bar/edit/outline/fold_unfold_all"] = "折叠/展开全部代码";
		["menu_bar/edit/advanced"] = "高级";
		["menu_bar/edit/advanced/comment_selection"] = "注释选中内容";
		["menu_bar/edit/advanced/uncomment_selection"] = "取消注释选中内容";
		["menu_bar/edit/advanced/format_selection"] = "格式化选中内容";
		["menu_bar/edit/advanced/wrap_selection_with_parenthesis"] = "()括起选中内容";
		["menu_bar/edit/advanced/wrap_selection_with_brackets"] = "{}括起选中内容";
		["menu_bar/edit/advanced/wrap_selection_with_squire_brackets"] = "[]括起选中内容";
		
		["menu_bar/project"] = "工程";
		["menu_bar/window"] = "窗口";
		["menu_bar/window/panes"] = "面板";
		["menu_bar/window/open/solution"] = "工程";
		["menu_bar/window/open/class"] = "类视图";
		["menu_bar/window/open/output"] = "输出";
		["menu_bar/window/open/search_result"] = "查找 && 替换";
		["menu_bar/window/open/build"] = "编译输出";
		["menu_bar/window/open/lsp"] = "语言服务";
		["menu_bar/window/open/debugger"] = "调试器";
		["menu_bar/window/open/locals"] = "局部变量";
		["menu_bar/window/open/watch"] = "监视";
		["menu_bar/window/open/stack"] = "调用栈";
		["menu_bar/window/open/threads"] = "线程";
		["menu_bar/window/open/modules"] = "模块";
		["menu_bar/window/open/breakpoints"] = "断点";
		["menu_bar/window/open/toolsets"] = "工具链";
		["menu_bar/window/next_file"] = "下一个文件(&N)";
		["menu_bar/window/previous_file"] = "上一个文件(&P)";
		["menu_bar/window/file_list"] = "文件列表(&P)";
		["menu_bar/window/fullscreen"] = "切换全屏";
		
		["menu_bar/help"] = "帮助";
		["menu_bar/help/help"] = "帮助";
		["menu_bar/help/find"] = "查找";
		["menu_bar/help/index"] = "索引";
		["menu_bar/help/register_shell_menu_shortcuts"] = "关联右键菜单";
		["menu_bar/help/about"] = "关于(&A)";

		["menu_bar/cmake"] = "CMake";
		["menu_bar/cmake/build_all"] = "生成全部";
		["menu_bar/cmake/rebuild_all"] = "重新生成全部";
		["menu_bar/cmake/build"] = "生成 ";
		["menu_bar/cmake/rebuild"] = "重新生成 ";
		["menu_bar/cmake/install"] = "安装";
		["menu_bar/cmake/refresh_cmake_cache"] = "更新缓存";
		["menu_bar/cmake/rebuild_cmake_cache"] = "重建缓存";
		["menu_bar/cmake/configuration"] = "配置类型";
		["menu_bar/cmake/toolsets"] = "工具链";
		["menu_bar/cmake/targets"] = "活动项目";

		["menu_bar/debug"] = "调试";
		["menu_bar/debug/build_and_run"] = "编译并运行";
		["menu_bar/debug/start_debug"] = "调试目标";
		["menu_bar/debug/continue"] = "继续运行";
		["menu_bar/debug/terminate"] = "停止运行";
		["menu_bar/cmake/step_over"] = "单步";
		["menu_bar/cmake/step_in"] = "步入";
		["menu_bar/cmake/step_out"] = "步出";
		["menu_bar/cmake/toggle_break_point"] = "切换断点";
		["menu_bar/cmake/list_break_point"] = "列出断点";
		["menu_bar/cmake/jump_to"] = "跳转执行点";
		["menu_bar/cmake/break"] = "中断运行";

		["dialog/line_end/select_line_end"] = "选择行尾";

		["dialog/search/pattern"] = " 查  找:";
		["dialog/search/replace_to"] = " 替  换:";
		["dialog/search/find_and_replace"] = "查找 & 替换";
		["dialog/search/case_insensitive"] = "忽略大小写";
		["dialog/search/use_regexp"] = "正则表达式";
		["dialog/search/find_up"] = "向上查找";
		["dialog/search/whold_word"] = "整个单词";
		["dialog/search/ranges"] = " 范  围:";
		["dialog/search/types"] = " 类  型:";
		["dialog/search/path"] = " 路  径:";
		["dialog/search/do_search"] = "查找";
		["dialog/search/do_search_all"] = "查找全部";
		["dialog/search/do_replace"] = "替换";
		["dialog/search/do_replace_all"] = "替换全部";
		["dialog/search/range_selection"] = "选中内容";
		["dialog/search/range_current_file"] = "当前文件";
		["dialog/search/range_opened_files"] = "所有打开的文件";
		["dialog/search/range_files"] = "所有文件";

		["dialog/about"] = "关 于";
		
		["dialog/delete_file"] = "确认删除文件?";
		["dialog/delete_file/delete_selected_files"] = "正在删除选中的[%d]个文件!";
		["dialog/delete_file/remove"] = "移除(&R)";
		["dialog/delete_file/delete"] = "删除(&D)";
		["dialog/delete_file/cancel"] = "取消(&C)";
		
		["dialog/file_changes/reload_all"] = "更新全部";
		["dialog/file_changes/cancel"] = "取消(&C)";
		["dialog/file_changes/reload"] = "更新(&R)";
		["dialog/file_changes/file_changed"] = "文件己更改!";
		["dialog/file_changes/files_changed"] = "%lld 个文件己更改!";
		["dialog/file_changes/file_change_notify"] = "文件 [%ls] 已被更改!\n是否更新文件?";

		["dialog/notify/ok"] = "确定(&O)";
		["dialog/notify/cancel"] = "取消(&C)";
		["dialog/notify/update"] = "更新(&U)";
		["dialog/file/all_file_types"] = "全部文件类型";
		
		["pane/output/title"] = "输 出";
		["pane/find_and_replace/title"] = "查找 & 替换";
		["pane/build_results/title"] = "编 译";
		["pane/build_results/all_errors"] = "所有错误";
		["pane/build_results/all_warnings"] = "所有警告";
		["pane/lsp/title"] = "语言服务";
		["pane/debugger/title"] = "调试器";
		["pane/watches/title"] = "监 视";
		["pane/locals/title"] = "变 量";
		["pane/threads/title"] = "线 程";
		["pane/stackframe/title"] = "调用栈";
		["pane/modules/title"] = "模 块";
		["pane/solution/title"] = "工 程";
		["pane/class/title"] = "类";
		["pane/resource/title"] = "资 源";
		["pane/properties/title"] = "属 性";
		
		["status/idle"] = "空闲";
		["status/selection_size"] = "选中字符数";
		["status/incremental_search"] = "增量搜索:";
		["status/incremental_search_failed"] = "增量搜索错误:";
		["status/goto_line"] = "跳转到行:";
		["status/download_update"] = "正在更新...";

		["shell/open_with_edx"] = "用&EDX打开";
		
	};
	["en-us"] = {
		["sln/popup/open"] = "Open";
		["sln/popup/new"] = "New";
		["sln/popup/new/file"] = "File";
		["sln/popup/new/folder"] = "Folder";
		["sln/popup/open_file_dir"] = "Open directory";
		["sln/popup/open_terminal"] = "Open terminal";
		["sln/popup/close_files_in_dir"] = "Close files in directory";
		["sln/popup/cut"] = "Cut(&X)";
		["sln/popup/copy"] = "&Copy";
		["sln/popup/paste"] = "Paste(&V)";
		["sln/popup/delete"] = "&Delete";
		["sln/popup/rename"] = "&Rename";
		["sln/popup/property"] = "Property(&A)";
		["sln/popup/git"] = "GIT";
		["sln/popup/git/pull"] = "Pull";
		["sln/popup/git/push"] = "Push";
		["sln/popup/git/history"] = "History";
		["sln/popup/git/gui"] = "GUI";

		["text/popup/jump_to_file"] = "Goto file(&F)";
		["text/popup/jump_to_definition"] = "Goto &defeinition/declaration";
		["text/popup/find_implementation"] = "Find implementation";
		["text/popup/find_reference"] = "Find reference";
		["text/popup/close_other_files"] = "Close all other files";
		["text/popup/open_directory"] = "Open directory";
		["text/popup/open_terminal"] = "Open terminal";
		["text/popup/cut"] = "Cut(&X)";
		["text/popup/copy"] = "&Copy";
		["text/popup/paste"] = "Paste(&V)";
		["text/popup/select_all"] = "Select &All";

		["utils/file_format_popup/reload_as"] = "Reload with encoding";
		["utils/file_format_popup/save_as"] = "Save with encoding";

		["notify/build/still_running"] = "Building...";
		["notify/build/success"] = "Build success!";
		["notify/build/failed"] = "Build failed!";
		["notify/build/installed"] = "Installed!";
		["notify/build/install_failed"] = "Install failed!";
		["notify/build/cmake_cache_rebuilt"] = "CMake cache rebuilt!";
		["notify/build/cmake_cache_rebuild_failed"] = "CMake cache rebuild failed!";
		["notify/build/cmake_cache_refreshed"] = "CMake cache refreshed!";
		["notify/build/cmake_cache_refresh_failed"] = "CMake cache refresh failed!";

		["notify/debug/invalid_target"] = "Invalid debuggee!";
		["notify/debug/target_not_exists"] = "Debuggee not exists!";

		["notify/register_shortcuts/success"] = "Shell menu registered!";
		["notify/register_shortcuts/failed"] = "Register shell menu failed!";

		["notify/error"] = "Error";
		["notify/error/open_file_failed"] = "Open document failed!";
		["notify/error/save_file_failed"] = "Save document failed!";
		["notify/error/reload_file_failed"] = "Reload document failed!";
		["notify/error/update_failed"] = "Failed to update! Please retry later!";
		
		["notify/notice"] = "Notice";
		["notify/notice/save_changed_file"] = "File [%ls] has changed! Save it?";
		["notify/notice/overwrite_readonly_file"] = "File is readonly, overwrite?";
		["notify/notice/create_new_file"] = "File is not exists, create it?";
		["notify/notice/searching_text_not_found"] = "The following specified text was not found:\n\n";

		["notify/invalid_license"] = "Invalid license! Exiting!";
		["notify/license_expires"] = "License expires! Exiting!";
		["notify/internal_error"] = "Internal error! Exiting!";
		["notify/edx_out_of_data"] = "EDX is out of date! Exiting!";
		["notify/found_new_version"] = "Found new version %ls!";
		["notify/update_now"] = "EDX is ready to update.";
		["notify/old_target_not_found"] = "Build target [%s] not found!";
		["notify/new_target_selected"] = "Target [%s] was selected.";

		["menu_bar/file"] = "File";
		["menu_bar/file/new_file"] = "&New file";
		["menu_bar/file/open"] = "&Open...";
		["menu_bar/file/open_in_current_buffer"] = "Open in current &Window...";
		["menu_bar/file/close_file"] = "&Close file";
		["menu_bar/file/close_project"] = "Close project";
		["menu_bar/file/save"] = "&Save";
		["menu_bar/file/save_as"] = "Save &As...";
		["menu_bar/file/save_all"] = "Save all";
		["menu_bar/file/open_config_file"] = "Edit configuration";
		["menu_bar/file/recently_files"] = "Recently files...";
		["menu_bar/file/recently_projects"] = "Recently projects...";
		["menu_bar/file/exit"] = "E&xit";

		["menu_bar/edit"] = "Edit";
		["menu_bar/edit/undo"] = "&Undo";
		["menu_bar/edit/redo"] = "&Redo";
		["menu_bar/edit/cut"] = "Cut(&X)";
		["menu_bar/edit/copy"] = "&Copy";
		["menu_bar/edit/paste"] = "&Paste";
		["menu_bar/edit/circulate_paste"] = "Circulate paste";
		["menu_bar/edit/delete"] = "&Delete";
		["menu_bar/edit/select_all"] = "Select &All";
		["menu_bar/edit/find"] = "Search(&S)";
		["menu_bar/edit/find/incremental"] = "&Incremental...";
		["menu_bar/edit/find/find_and_replace"] = "&Find/Replace...";
		["menu_bar/edit/find/find_in_files"] = "Find in files...";
		["menu_bar/edit/jump_to"] = "Goto";
		["menu_bar/edit/jump_to/line"] = "Line...";
		["menu_bar/edit/jump_to/previous_location"] = "Previous location";
		["menu_bar/edit/jump_to/next_location"] = "Next location";
		["menu_bar/edit/jump_to/matched_brackets"] = "Matched brackets";
		["menu_bar/edit/jump_to/file"] = "File";
		["menu_bar/edit/jump_to/definition"] = "Definition/declaration";
		["menu_bar/edit/jump_to/implementation"] = "Implementation";
		["menu_bar/edit/jump_to/reference"] = "Reference";
		["menu_bar/edit/outline"] = "&Outline";
		["menu_bar/edit/outline/fold_unfold"] = "Fold/Unfold blocks";
		["menu_bar/edit/outline/fold_unfold_all"] = "Fold/Unfold all blocks";
		["menu_bar/edit/advanced"] = "Advanced";
		["menu_bar/edit/advanced/comment_selection"] = "Comment selection";
		["menu_bar/edit/advanced/uncomment_selection"] = "Uncomment selection";
		["menu_bar/edit/advanced/format_selection"] = "Format selection";
		["menu_bar/edit/advanced/wrap_selection_with_parenthesis"] = "Wrap selection with ()";
		["menu_bar/edit/advanced/wrap_selection_with_brackets"] = "Wrap selection with {}";
		["menu_bar/edit/advanced/wrap_selection_with_squire_brackets"] = "Wrap selection with []";

		["menu_bar/project"] = "Project";
		["menu_bar/window"] = "Window";
		["menu_bar/window/panes"] = "Panes";
		["menu_bar/window/open/solution"] = "Solution";
		["menu_bar/window/open/class"] = "Class";
		["menu_bar/window/open/output"] = "Output";
		["menu_bar/window/open/search_result"] = "Find && Replace";
		["menu_bar/window/open/build"] = "Build";
		["menu_bar/window/open/lsp"] = "LSP";
		["menu_bar/window/open/debugger"] = "Debugger";
		["menu_bar/window/open/locals"] = "Locals";
		["menu_bar/window/open/watch"] = "Watch";
		["menu_bar/window/open/stack"] = "Call Stack";
		["menu_bar/window/open/threads"] = "Threads";
		["menu_bar/window/open/modules"] = "Modules";
		["menu_bar/window/open/breakpoints"] = "Breakpoints";
		["menu_bar/window/open/toolsets"] = "Toolsets";
		["menu_bar/window/next_file"] = "&Next file";
		["menu_bar/window/previous_file"] = "&Previous file";
		["menu_bar/window/file_list"] = "File list";
		["menu_bar/window/fullscreen"] = "Fullscreen";
		

		["menu_bar/help"] = "Help";
		["menu_bar/help/help"] = "Help";
		["menu_bar/help/find"] = "Find";
		["menu_bar/help/index"] = "Index";
		["menu_bar/help/register_shell_menu_shortcuts"] = "Register the shell menu";
		["menu_bar/help/about"] = "About(&A)";

		["menu_bar/cmake"] = "CMake";
		["menu_bar/cmake/build_all"] = "Build all";
		["menu_bar/cmake/rebuild_all"] = "Rebuild all";
		["menu_bar/cmake/build"] = "Build";
		["menu_bar/cmake/rebuild"] = "Rebuild";
		["menu_bar/cmake/install"] = "Install";
		["menu_bar/cmake/refresh_cmake_cache"] = "Refresh CMake cache";
		["menu_bar/cmake/rebuild_cmake_cache"] = "Rebuild CMake cache";
		["menu_bar/cmake/configuration"] = "Configuration";
		["menu_bar/cmake/toolsets"] = "Toolsets";
		["menu_bar/cmake/targets"] = "Targets";

		["menu_bar/debug"] = "Debug";
		["menu_bar/debug/build_and_run"] = "Build && Run";
		["menu_bar/debug/start_debug"] = "Start debug";
		["menu_bar/debug/continue"] = "Continue";
		["menu_bar/debug/terminate"] = "Terminate";
		["menu_bar/cmake/step_over"] = "Step over";
		["menu_bar/cmake/step_in"] = "Step in";
		["menu_bar/cmake/step_out"] = "Step out";
		["menu_bar/cmake/toggle_break_point"] = "Toggle breakpoint";
		["menu_bar/cmake/list_break_point"] = "List breakpoints";
		["menu_bar/cmake/jump_to"] = "Jump to";
		["menu_bar/cmake/break"] = "Break";
		
		["dialog/line_end/select_line_end"] = "Select line end";
		
		["dialog/search/pattern"] = "Find &what:";
		["dialog/search/replace_to"] = "Replace:";
		["dialog/search/find_and_replace"] = "Find & Replace:";
		["dialog/search/case_insensitive"] = "Ignore &case";
		["dialog/search/use_regexp"] = "Use r&egexp";
		["dialog/search/find_up"] = "Fin&d up";
		["dialog/search/whold_word"] = "Wh&ole word";
		["dialog/search/ranges"] = "Ranges:";
		["dialog/search/types"] = "Types:";
		["dialog/search/path"] = "Path:";
		["dialog/search/do_search"] = "Find";
		["dialog/search/do_search_all"] = "Find All";
		["dialog/search/do_replace"] = "Replace";
		["dialog/search/do_replace_all"] = "Replace All";
		["dialog/search/range_selection"] = "Selection";
		["dialog/search/range_current_file"] = "Current File";
		["dialog/search/range_opened_files"] = "Opened Files";
		["dialog/search/range_files"] = "Files";

		["dialog/about"] = "About";

		["dialog/delete_file"] = "Confirm deleting";
		["dialog/delete_file/delete_selected_files"] = "Deleting selected [%d] file(s)!";
		["dialog/delete_file/remove"] = "&Remove";
		["dialog/delete_file/delete"] = "&Delete";
		["dialog/delete_file/cancel"] = "&Cancel";

		["dialog/file_changes/reload_all"] = "Reload All";
		["dialog/file_changes/cancel"] = "&Cancel";
		["dialog/file_changes/reload"] = "&Reload";
		["dialog/file_changes/file_changed"] = "File has been modified!";
		["dialog/file_changes/files_changed"] = "%lld files has been modified!";
		["dialog/file_changes/file_change_notify"] = "File [%ls] has been modified!\nReload the file?";

		["dialog/notify/ok"] = "&OK";
		["dialog/notify/cancel"] = "&Cancel";
		["dialog/notify/update"] = "&Update";
		["dialog/file/all_file_types"] = "All File Types";

		["pane/output/title"] = "Output";
		["pane/find_and_replace/title"] = "Find & Replace";
		["pane/build_results/title"] = "Build Results";
		["pane/build_results/all_errors"] = "All Error(s)";
		["pane/build_results/all_warnings"] = "All Warning(s)";
		["pane/lsp/title"] = "LSP";
		["pane/debugger/title"] = "Debugger";
		["pane/watches/title"] = "Watches";
		["pane/locals/title"] = "Locals";
		["pane/threads/title"] = "Threads";
		["pane/stackframe/title"] = "StackFrame";
		["pane/modules/title"] = "Modules";
		["pane/solution/title"] = "Solution Explorer";
		["pane/class/title"] = "Class";
		["pane/resource/title"] = "Resource";
		["pane/properties/title"] = "Properties";

		["status/idle"] = "Idle";
		["status/selection_size"] = "Selection Size";
		["status/incremental_search"] = "Incremental Search:";
		["status/incremental_search_failed"] = "Incremental Search failed:";
		["status/goto_line"] = "Goto Line:";
		["status/download_update"] = "Updating...";
		
		["shell/open_with_edx"] = "Open with &EDX";
	};
};

LANGUAGE["default"] = LANGUAGE["en-us"];
