package.path = "./lua/?.lua;"..package.path
math.randomseed(math.floor(os.clock()));

mgr = edx.document_manager;
xws_mgr = edx.xws_mgr;

print = function( ... )
	for i, msg in ipairs({...}) do
		edx.output( tostring(msg) );
	end;
	edx.output( "\r\n" );
end;

function dbg_call( func, err_msg, finally_func )
	local result = {
		xpcall( func,
			function(msg)
				if err_msg ~= nil then
					print( err_msg );
				end;
				msg = debug.traceback( msg );
				msg = msg:gsub( "\n", "\r\n" );
				print( msg );
				print( "\r\n" );
				if finally_func then
					finally_func();
				end;
				return 0;
			end
		)
	};
	if result[1] == true then
		table.remove( result, 1 );
		return table.unpack(result);
	end;
end;

function k( name )
	return edx.key_from_name( name );
end;
function kname( key )
	return edx.key_name( key );
end;
function icon( name )
	return edx.get_icon( name );
end;

edx:clear("output");

-- basic key binding
edx:register_short_cuts(-1,{50, k("ctrl+s")}); -- save
edx:register_short_cuts(-1,{53, k("ctrl+o")}); -- open
edx:register_short_cuts(-1,{55, k("ctrl+n")}); -- new doc

edx:register_short_cuts(1,{74, k("ctrl+f")}); -- find
edx:register_short_cuts(1,{75, k("ctrl+f")}); -- find next
edx:register_short_cuts(1,{76, k("ctrl+z")}); -- undo
edx:register_short_cuts(1,{77, k("ctrl+y")}); -- redo
edx:register_short_cuts(1,{64, k("ctrl+x")}); -- cut
edx:register_short_cuts(1,{65, k("ctrl+v")}); -- paste
edx:register_short_cuts(1,{28, k("del")}); -- delete
edx:register_short_cuts(1,{29, k("backspace")}); -- backspace
edx:register_short_cuts(1,{45, k("ctrl+a")}); -- select all
edx:register_short_cuts(1,{31, k("escape")}); -- unselect
edx:register_short_cuts(1,{1, k("ctrl+left")}); -- enhanced_left
edx:register_short_cuts(1,{2, k("ctrl+right")}); -- enhanced_right
edx:register_short_cuts(1,{3, k("ctrl+shift+left")}); -- enhanced_left_select
edx:register_short_cuts(1,{4, k("ctrl+shift+right")}); -- enhanced_right_select
edx:register_short_cuts(1,{7, k("left")}); -- cursor_left
edx:register_short_cuts(1,{8, k("right")}); -- cursor_right
edx:register_short_cuts(1,{10, k("down")}); -- cursor_down
edx:register_short_cuts(1,{9, k("up")}); -- cursor_up
edx:register_short_cuts(1,{5, k("home")}); -- enhanced_home
edx:register_short_cuts(1,{6, k("shift+home")}); -- enhanced_home_select
edx:register_short_cuts(1,{11, k("end")}); -- cursor_end
edx:register_short_cuts(1,{21, k("shift+end")}); -- cursor_end_select
edx:register_short_cuts(1,{13, k("pageup")}); -- cursor_page_up
edx:register_short_cuts(1,{14, k("pagedown")}); -- cursor_page_down
edx:register_short_cuts(1,{17, k("shift+left")}); -- cursor_left_select
edx:register_short_cuts(1,{18, k("shift+right")}); -- cursor_right_select
edx:register_short_cuts(1,{19, k("shift+down")}); -- cursor_down_select
edx:register_short_cuts(1,{20, k("shift+up")}); -- cursor_up_select

function edx:on_locate_output(info)
	local out = {}
	if out[1] == nil or out[2] == nil then
		out = { info:match("^%s*(doc://<[%d%w]+>):(%d+):") };
		out[3] = 1;
	end;
	if out[1] == nil or out[2] == nil then
		out = { info:match("^%s*([a-zA-Z]?:?[^<>?*:;'\"]+):(%d+):") };
		out[3] = 1;
	end;
	if out[1] == nil or out[2] == nil then
		return 0;
	end ;
	local file_name = out[1];
	if mgr.open_doc(file_name) then
		mgr.record_cursor_history();
		local cur_doc = mgr.current_document;
		cur_doc:set_cursor(tonumber(out[2]) - 1, tonumber(out[3]) - 1);
		edx.do_cmd(_op.center_cursor_line);
	end;
end;

dbg_call(
	function()
		require "startup";
	end
	, "initialize failed"
	, function()
		edx:active("output");
	end
);
