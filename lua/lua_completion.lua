lua_completion = {
	[0] = {
		"io", "os", "print", "assert", "collectgarbage", "dofile", "error", "getfenv", "getmetatable", "ipairs", "load", "loadfile",
		"loadstring", "next", "pairs", "pcall", "rawequal", "rawget", "rawset", "select", "setfenv", "setmetatable",
		"tonumber", "tostring", "type", "unpack", "_VERSION", "xpcall", "math", "string", "table"
	},
	["io"] = { "close", "flush", "input", "lines", "open", "output", "popen", "read", "tmpfile", "type", "write" },
	["os"] = { "clock", "date", "difftime", "execute", "exit", "getenv", "remove", "rename", "setlocale", "time", "tmpname" },
	["table"] = { "concat", "insert", "maxn", "remove", "sort" },
	["string"] = { "byte", "char", "dump", "find", "format", "gmatch", "gsub", "len", "lower", "match", "rep", "reverse", "sub", "upper" },
	["math"] = {
		"abs", "acos", "atan", "atan2", "ceil", "cos", "cosh", "deg", "exp", "floor", "fmod", "frexp", "huge", "ldexp", "log",
		"log10", "max", "min", "modf", "pi", "pow", "rad", "random", "randomseed", "sin", "sinh", "sqrt", "tan", "tanh"
	}
};

function make_edx_completion()
	local edx_members = {};
	local info = lua_ex:class_info(edx);
	for i, v in ipairs(info.member) do
		table.insert(edx_members, v[2]);
	end;
	for i, v in ipairs(info.event) do
		table.insert(edx_members, v[2]);
	end;
	lua_completion["edx"] = edx_members;

	local mgr_members = {};
	local info = lua_ex:class_info(mgr);
	for i, v in ipairs(info.member) do
		table.insert(mgr_members, v[2]);
	end;
	for i, v in ipairs(info.event) do
		table.insert(mgr_members, v[2]);
	end;
	lua_completion["mgr"] = mgr_members;

	local xws_mgr_members = {};
	local info = lua_ex:class_info(xws_mgr);
	for i, v in ipairs(info.member) do
		table.insert(xws_mgr_members, v[2]);
	end;
	for i, v in ipairs(info.event) do
		table.insert(xws_mgr_members, v[2]);
	end;
	lua_completion["xws_mgr"] = xws_mgr_members;
end;

function mgr:on_lua_completion(doc, status, helper)
	-- lua 自动补全
	-- print("lua complete ", status);
	-- 得到当前行
	local text = doc.get_text(doc.cursor_line, 0, doc.cursor_column);
	if text == nil then
		helper.cancel_completion();
		return 0;
	end ;
	-- 取得最后一部分
	text = text:match("([.:_%w]+)$");
	if status ~= 0 and (text == nil or text == "") then
		helper.cancel_completion();
		return 0;
	end ;
	local class = text:match("([^.:]+).*");
	local context = text:match(".*[.:]([^.:]*)");

	if context == nil then
		context = class;
		class = "";
	end ;
	if status == 1 then
		-- 选择当前项
		doc.select_text(doc.cursor_line, doc.cursor_column - sizeof(context), doc.cursor_line, doc.cursor_column);
		doc.set_text(helper.selected_item_text);
		helper.cancel_completion();
		return 1;
	end ;
	if status == 0 or status == 2 then
		helper.clear(false);
		local info_class = lua_completion[class];
		if info_class == nil then
			info_class = lua_completion[0];
		end ;
		for i, v in pairs(info_class) do
			helper.push_back(v, 0, 0);
		end ;
		helper.sort_items();
		helper.select_item(context);
		helper.redraw();
		return 1;
	end ;

	return 0;
end;
