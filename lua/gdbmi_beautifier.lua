local beautifiers = {
	-- std::string
	{
		-- pattern=[[^std::(string|wstring)$]];
		handler=function(self, id, type_name, sym)
			sym:add_member_map("[raw]", nil);
			local size_sym = sym:add_member_map("size", {"_M_string_length"});
			size_sym.simple_value = true;
			local value = sym:add_member_map("ptr", {"_M_dataplus","_M_p"});
			-- make ptr as the string's value member
			sym:set_display_mem_value("ptr");
			-- override value with custom format
			sym:set_display_value(([[{size=%d,%s}]]):format(size_sym.value, value.value));
			return true;
		end;
	};
};

gdbmi_beautifier = edx.get_debugger_beautifier("gdb/mi");

function gdbmi_beautifier.on_beautify_symbol(self, id, type_name, sym)
	local handler = self.__beautifiers[id];
	if not handler then
		return false;
	end;
	return handler(id, type_name, sym);
end;

local gdbmi_beautifier_map = {};

for i, obj in ipairs(beautifiers) do
	if obj.pattern then
		local idx = gdbmi_beautifier:add_beautify_pattern(obj.pattern);
		gdbmi_beautifier_map[idx] = (function(the_obj)
				return function(...)
					return the_obj:handler(...);
				end;
		end)(obj);
	end;
end;

gdbmi_beautifier.__beautifiers = gdbmi_beautifier_map;

print( "load gdb/mi beautifier ok!");
