# EDX 高性能可扩展编辑器

#### 介绍
此仓库为EDX编辑器的官方二进制仓库。EDX核心并未开源。

仓库主要用于收集反馈，建议，跟踪BUG。接收第三方扩展脚本。

[EDX官方网站](https://www.ed-x.cc)
=====================================================

## EDX是一款高性能的可扩展编辑器

- 它体积小巧，绿色无污染，开箱即用。
- 能快速打开大型文件，轻松编辑有千万行内容的文本
- 支持C/C++，LUA，JavaScript，Java，D，HTML，XML，CSS，CMake，JSON等格式的高亮显示
- 内建C/C++调试器。支持Win32/64及Linux程序的调试
- 内建lua脚本，用户可自由扩展功能
- 内建对MSVC，MSYS2/Mingw，clang，Intel oneAPI，WSL，llvm-mingw等工具集的支持
- C/C++支持基于clangd的语法提示，补全，跳转等基本操作

### CMake工程内使用Clangd进行代码补全

![代码补全](https://www.ed-x.cc/imgs/code-completion.png)

### WSL下调试代码

![WSL下调试代码](https://www.ed-x.cc/imgs/wsl-debugging.png)

### 无需建立工程，立即模式下直接编译调试单个源代码

![立即模式下编译](https://www.ed-x.cc/imgs/instant-build.png)
![立即模式下编译](https://www.ed-x.cc/imgs/instant-debug.png)
